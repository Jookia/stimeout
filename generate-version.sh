#!/bin/sh

# variables subsittuted on git archive
EXPORTED='$Format:yes$'
EXPORTREFS='$Format:%D$'
EXPORTCOMMIT='$Format:%h$'

if test "x$EXPORTED" = "xyes"; then
  TAGVER="$(echo "$EXPORTREFS" | sed -E "s|^(.* )?tag: ([^,]*).*$|\2|")"
  if test "x$TAGVER" != "x$EXPORTREFS"; then
    VERSION="$TAGVER"
  else
    VERSION="$EXPORTCOMMIT"
  fi
else
  VERSION="$(git describe --dirty)"
fi

printf > version.h '#define BUILD_VERSION "%s"' $VERSION
echo "version $VERSION"
