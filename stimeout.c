/* Written 2019-10-04 by Jookia and placed in the public domain */

#define _POSIX_C_SOURCE 1
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "version.h"

/* Allocate memory here, not the stack */
int wstatus = 0;
int alarmed = 0;
int timeout = 0;
struct sigaction action;

void alarm_handler(int signal) {
  if(signal == SIGALRM)
    alarmed = 1;
}

int main(int argc, char** argv) {
  /* Print the version if requested */
  if(argc > 1 && strcmp(argv[1], "--version") == 0) {
    fprintf(stdout, "stimeout %s\n", BUILD_VERSION);
    return 0;
  }
  /* Check we have timeout and binary args */
  if (argc < 3) {
    fprintf(stderr, "Usage: %s [--version] timeout_secs program [args]\n", argv[0]);
    return 1;
  }
  /* Parse the timeout */
  if(sscanf(argv[1], "%i", &timeout) != 1) {
    fprintf(stderr, "%s: Unable to read timeout number: %s\n", argv[0], argv[1]);
    return 1;
  }
  if(timeout < 1) {
    fprintf(stderr, "%s: Invalid timeout: %i\n", argv[0], timeout);
    return 1;
  }
  /* Fork and run the process */
  switch(fork()) {
    case 0:
      execvp(argv[2], &argv[2]);
      fprintf(stderr, "%s: Unable to exec %s: %s\n", argv[0], argv[2], strerror(errno));
      return 1;
    case -1:
      fprintf(stderr, "%s: Unable to fork: %s\n", argv[0], strerror(errno));
      return 1;
    default:
      break;
  }
  /* Set up an alarm handler for our timeout */
  action.sa_handler = alarm_handler;
  action.sa_flags = 0;
  if(sigaction(SIGALRM, &action, NULL) == -1) {
      fprintf(stderr, "%s: Unable to set up alarm handler: %s\n", argv[0], strerror(errno));
      return 1;
  }
  alarm(timeout);
  /* Wait for the child to exit */
  switch(waitpid(-1, &wstatus, 0)) {
    case -1:
      if(errno == EINTR && alarmed) {
        /* Still running after timeout interruption */
        return 0;
      } else {
        fprintf(stderr, "%s: Unable to waitpid: %s\n", argv[0], strerror(errno));
        return 1;
      }
    default:
      /* Exited before timeout */
      return WEXITSTATUS(wstatus);
  }
}
