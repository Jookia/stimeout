#!/bin/sh
# Written 2019-10-04 by Jookia and placed in the public domain

set -ve

# Test argument parsing
./stimeout 2>/dev/null && exit 1
./stimeout 2>&1 | grep -q "Usage:" || exit 1
./stimeout test 2>/dev/null && exit 1
./stimeout test 2>&1 | grep -q "Usage:" || exit 1
./stimeout --version >/dev/null || exit 1
./stimeout --version | grep -q "^stimeout [a-z0-9.]*$" || exit 1

# Test timeout parsing
./stimeout 0 true 2>/dev/null && exit 1
./stimeout 0 true 2>&1 | grep -q "Invalid timeout" || exit 1
./stimeout -1 true 2>/dev/null && exit 1
./stimeout -1 true 2>&1 | grep -q "Invalid timeout" || exit 1
./stimeout hello true 2>/dev/null && exit 1
./stimeout hello true 2>&1 | grep -q "Unable to read timeout" || exit 1

# Test that fake executables don't work
./stimeout 1 notarealexecutable 2>/dev/null && exit 1
./stimeout 1 notarealexecutable 2>&1 | grep -q "Unable to exec" || exit 1

# Test that working executables work
./stimeout 1 echo Hello || exit 1
./stimeout 1 echo Hello | grep -q "Hello" || exit 1

# Test that fails are caught and return proper codes
./stimeout 1 /bin/sh -c "exit 42" && exit 1
test $? -eq 42 || exit 1

# Test that fails after timeouts aren't caught
./stimeout 1 /bin/sh -c "sleep 2; exit 42" || exit 1
test $? -eq 0 || exit 1

# All good!
exit 0
