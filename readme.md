The problem
-----------

In systemd, simple services don't report failure, even if it happens quickly.
There's also no way to ask for timeout for simple service failures.

Consider this service in systemd:

```
[Unit]
Description=My dumb unit

[Service]
ExecStart=/bin/sh -c "sleep 1; exit 42"
Type=simple
Before=important.service
RequiredBy=important.service
```

Despite failing after one second, important.service will still start.
This is because starting a service of type simple is immediately considered
successful if the process continues to run.

systemd offers basically two ways to fix this:

- Notify success using sd_notify or dbus
- Notify success using a main process that starts the daemon

Both of these require the application to be patched to add these.

The solution
------------

stimeout will report whether the application is alive after a time period.
This is done using the traditional forking model. stimeout will:

- Fork the application
- Wait up to a timeout in seconds
- Exit with code 0 if the application is still running, or:
- Exit with the application's exit code

All you have to do is set the service's type to forking and prefix the
application's command with stimeout and the time to wait in seconds.

Here's the previous service with a timeout of 5 seconds:

```
[Unit]
Description=My dumb unit

[Service]
ExecStart=stimeout 5 /bin/sh -c "sleep 1; exit 42"
Type=forking
Before=important.service
RequiredBy=important.service
```

After one second stimeout will report the application has failed and
important.service will not run.

Is this a good idea?
--------------------

Not really, applications aren't guaranteed to take a specific amount of time to
reach a failure point. This is likely why systemd doesn't support this.

But it's better than nothing I guess?

Installation
------------

Install a C compiler on your Linux and run:

```
make
make check
sudo make install
```

License
-------

Public domain.
