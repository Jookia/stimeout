# Written 2019-10-04 by Jookia and placed in the public domain

CC?=gcc
CFLAGS?=-Wall -Wextra -std=c89
PREFIX?=/usr/local
DESTDIR?=

stimeout: stimeout.c
	./generate-version.sh
	$(CC) stimeout.c $(CFLAGS) -o stimeout

check: stimeout ./sanity-tests.sh
	./sanity-tests.sh

clean:
	rm stimeout version.h

install: stimeout
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install stimeout $(DESTDIR)$(PREFIX)/bin
